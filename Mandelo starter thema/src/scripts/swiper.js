// Import Swiper and modules
import {
	Swiper,
	Navigation,
	Pagination,
	Scrollbar,
	Controller
} from '../../config/node_modules/swiper/js/swiper.esm.js';
Swiper.use([Navigation, Pagination, Scrollbar, Controller]);